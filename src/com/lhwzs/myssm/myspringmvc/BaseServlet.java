package com.lhwzs.myssm.myspringmvc;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 这是一个公共的Servlet，这个类大家直接使用
 * 这个Servlet我们不需要直接访问，如果不抽取BaseServlet，那么所有请求都需要写一个XXXXServlet.java，文件太多，管理起来很乱
 * 
 * @author 7480
 *
 */
public class BaseServlet extends HttpServlet {

	/**
	 * 重写service()方法，然后再该方法分发请求
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("【已经进入到了后台服务器！！！！！】");
		// 分发方法也就是login,findAllUser，addUser等
		// 分发方法的实现
		// 1.获取请求路径
		String uri = req.getRequestURI();// /SelectCourse/user/login
		System.out.println("请求uri:" + uri);
		// 2.获取方法名称
		String methodName = uri.substring(uri.lastIndexOf("/") + 1);// login
		System.out.println("方法名称：" + methodName);
		// 3.获取方法对象Method,根据方法名称反射方法对象
		try {
			Method method = this.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);// login(HttpServletRequest																										// }
			// 4.执行方法
			method.invoke(this, req, resp);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
