package com.lhwzs.user.servlets;

import com.google.gson.Gson;
import com.lhwzs.myssm.myspringmvc.BaseServlet;
import com.lhwzs.user.dao.IUserDAO;
import com.lhwzs.user.dao.impl.ClassmessageImpl;
import com.lhwzs.user.pojo.Message;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Auther: 李燦
 * @Date: 2022/5/13 - 05 - 11:17
 * @Decsription: com.lhwzs.user.servlets
 * @version: 1.0
 */
@WebServlet("/message/*")
public class messageServlet extends BaseServlet {
    public void people(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        Map<String, Object> result = new HashMap<>();
        IUserDAO dao = new ClassmessageImpl();
        Integer userId=Integer.parseInt(request.getParameter("userId"));
        Message message = dao.getclassById(userId);
        if(message!=null){
            result.put("result",message);
        }
        response.getWriter().write(new Gson().toJson(result));
    }
}
