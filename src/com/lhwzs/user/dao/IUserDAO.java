package com.lhwzs.user.dao;

import com.lhwzs.user.pojo.User;
import com.lhwzs.user.pojo.Message;

import java.util.List;

public interface IUserDAO {
    //登录用户
    User login (User user);
    //获取指定页数的users列表信息
    List<User> getuserList(String keyword , Integer pageNo,Integer institute);

    //根据id获取特定的user信息
    User getuserById(Integer userId);

    //根据id进行多表查询
    Message getclassById(Integer userId);
    //修改指定的user
    int updateuser(User user);

    //根据id删除指定的users记录
    int deluser(Integer userId);

    //添加user
    int adduser(User user);
    //获取数据总条目数
    int getuserCount(String keyword,Integer institute);
    //根据最大用户编号获取用户的账号
    String getUsernameByMaxId();
    //查看是否存在此密码
    boolean mima(int userId,String oldPassword);
    //修改密码
    int updatepwd(int userId,String newPassword);
    //修改账号
    int updatenicheng(int userId,String username);
}
