package com.lhwzs.user.dao.impl;

import com.lhwzs.myssm.basedao.BaseDAO;
import com.lhwzs.user.dao.IUserDAO;
import com.lhwzs.user.pojo.Message;
import com.lhwzs.user.pojo.User;

import java.util.List;

/**
 * @Auther: 李燦
 * @Date: 2022/5/11 - 05 - 15:32
 * @Decsription: com.lhwzs.user.dao.impl
 * @version: 1.0
 */
public class ClassmessageImpl extends BaseDAO<Message> implements IUserDAO {
    @Override
    public User login(User user) {
        return null;
    }

    @Override
    public List<User> getuserList(String keyword, Integer pageNo, Integer institute) {
        return null;
    }

    @Override
    public User getuserById(Integer userId) {
        return null;
    }

    @Override
    public Message getclassById(Integer userId) {
        return super.load2("SELECT classId,classname,apartment,dormitory,nickName,u.sex,u.phone,u.specialty,c.userId FROM users u,class c WHERE u.userId=c.userId AND c.userId=?",userId);
    }

    @Override
    public int updateuser(User user) {
        return 0;
    }

    @Override
    public int deluser(Integer userId) {
        return 0;
    }

    @Override
    public int adduser(User user) {
        return 0;
    }

    @Override
    public int getuserCount(String keyword,Integer institute) {
        return 0;
    }

    @Override
    public String getUsernameByMaxId() {
        return null;
    }

    @Override
    public boolean mima(int userId, String oldPassword) {
        return false;
    }

    @Override
    public int updatepwd(int userId, String newPassword) {
        return 0;
    }

    @Override
    public int updatenicheng(int userId, String username) {
        return 0;
    }
}
