package com.lhwzs.user.dao.impl;

import com.lhwzs.user.dao.IUserDAO;
import com.lhwzs.myssm.basedao.BaseDAO;
import com.lhwzs.user.pojo.User;
import com.lhwzs.user.pojo.Message;

import java.util.List;
public class UserDAOImpl extends BaseDAO<User> implements IUserDAO {
    @Override
    public User login(User user) {
        String sql = "SELECT * FROM users WHERE username=? AND PASSWORD=?";
        return super.load(sql,user.getUsername(),user.getPassword());
    }

    @Override
    public List<User> getuserList(String keyword , Integer pageNo,Integer institute) {
        return super.executeQuery("SELECT * FROM users WHERE instituteId=? AND (nickName LIKE ? OR specialty LIKE ? OR sex LIKE ?) and role=1 LIMIT ?,5",institute,"%"+keyword+"%","%"+keyword+"%","%"+keyword+"%",(pageNo-1)*5);
    }

    @Override
    public User getuserById(Integer userId) {
        return super.load("select * from users where userId = ? " , userId);
    }

    @Override
    public Message getclassById(Integer userId) {
        return null;
    }

    @Override
    public int updateuser(User user) {
        String sql = "UPDATE users SET nickName=?,sex=?,phone=?,specialty=?,instituteId=? WHERE userId=?";
        return super.executeUpdate(sql,user.getNickName(),user.getSex(),user.getPhone(),user.getSpecialty(),user.getInstituteId(),user.getUserId());
    }

    @Override
    public int deluser(Integer userId) {
        return super.executeUpdate("delete from users where userId = ? " , userId) ;
    }

    @Override
    public int adduser(User user) {
        String sql = "insert into users(username,password,nickName,sex,phone,specialty,instituteId,role) values(?,?,?,?,?,?,?,1)";
        return super.executeUpdate(sql,user.getUsername(),user.getPassword(),user.getNickName(),user.getSex(),user.getPhone(),user.getSpecialty(),user.getInstituteId());
    }
    @Override
    public int getuserCount(String keyword,Integer institute) {
        return ((Long)super.executeComplexQuery("select count(*) from users where (nickName like ? or sex like ? or specialty like ?) and instituteId=? and role=1","%"+keyword+"%","%"+keyword+"%","%"+keyword+"%",institute)[0]).intValue();
    }

    @Override
    public String getUsernameByMaxId() {
        String sql = "SELECT username FROM users WHERE userId = (SELECT MAX(userId) max_user_id FROM users)";
        return super.getUsernameByMaxId(sql);
    }

    @Override
    public boolean mima(int userId, String oldPassword) {
        String sql="select * from users where userId = ? and password = ?";
        return super.checkOldPwd(sql,userId,oldPassword);
    }

    @Override
    public int updatepwd(int userId, String newPassword) {
        String sql="update users set password=? where userId =?";
        return super.executeUpdate(sql,newPassword,userId);
    }

    @Override
    public int updatenicheng(int userId, String username) {
        String sql="update users set username=? where userId =?";
        return super.executeUpdate(sql,username,userId);
    }

}
