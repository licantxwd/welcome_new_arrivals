package com.lhwzs.user.pojo;

/**
 * @Auther: 李燦
 * @Date: 2022/4/30 - 04 - 15:19
 * @Decsription: com.lhwzs.user.pojo
 * @version: 1.0
 */
public class User {
    private Integer userId;
    private String username;
    private String password;
    private String nickName;
    private String sex;
    private String phone;
    private String specialty;
    private Integer instituteId;
    private Integer role;

    public User() {
    }

    public User(Integer userId, String nickName, String sex, String phone, String specialty, Integer instituteId) {
        this.userId = userId;
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
        this.instituteId = instituteId;
    }

    public User(String nickName, String sex, String phone, String specialty) {
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
    }

    public User(Integer userId, String username, String password, String nickName, String sex, String phone, String specialty) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
    }

    public User(Integer userId, String username, String password, String nickName, String sex, String phone, String specialty, Integer instituteId) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
        this.instituteId = instituteId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Integer getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Integer instituteId) {
        this.instituteId = instituteId;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nickName='" + nickName + '\'' +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                ", specialty='" + specialty + '\'' +
                ", instituteId=" + instituteId +
                ", role=" + role +
                '}';
    }
}
