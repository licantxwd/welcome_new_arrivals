package com.lhwzs.user.pojo;

/**
 * @Auther: 李燦
 * @Date: 2022/5/11 - 05 - 14:56
 * @Decsription: com.lhwzs.user.pojo
 * @version: 1.0
 */
public class Message {
    private Integer userId;
    private Integer classId;
    private String nickName;
    private String sex;
    private String phone;
    private String specialty;
    private String classname;
    private String apartment;
    private Integer dormitory;
    public Message(){

    }
    public Message(String nickName, String sex, String phone, String specialty, String classname, String apartment, Integer dormitory) {
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
        this.classname = classname;
        this.apartment = apartment;
        this.dormitory = dormitory;
    }
    public Message(Integer userId, Integer classId, String nickName, String sex, String phone, String specialty, String classname, String apartment, Integer dormitory) {
        this.userId = userId;
        this.classId = classId;
        this.nickName = nickName;
        this.sex = sex;
        this.phone = phone;
        this.specialty = specialty;
        this.classname = classname;
        this.apartment = apartment;
        this.dormitory = dormitory;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public Integer getDormitory() {
        return dormitory;
    }

    public void setDormitory(Integer dormitory) {
        this.dormitory = dormitory;
    }

    @Override
    public String toString() {
        return "Message{" +
                "userId=" + userId +
                ", classId=" + classId +
                ", nickName='" + nickName + '\'' +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                ", specialty='" + specialty + '\'' +
                ", classname='" + classname + '\'' +
                ", apartment='" + apartment + '\'' +
                ", dormitory=" + dormitory +
                '}';
    }
}
